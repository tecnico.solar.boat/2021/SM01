/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include "AS5045.h"
#include "CAN_TSB_H2.h" // substituir por Can h2 
#include <Timer.h>
#include "Config.h"
#include "VescCAN.h"

// CHANGE THESE AS APPROPRIATE
#define CSpin   14
#define CLKpin  18
#define DOpin   19

AS5045 enc (CSpin, CLKpin, DOpin) ;

TSB_CAN_H2 Can_tsb; // Can_tsb.motor1.voltage < 20 -> neutral_to_start 

CAN_message_t can_message;

VescCAN motor1(MOTOR1_ID);

int angle;
int start_angle;
int start_angle2;
int8_t throttle;
uint8_t status;
bool neutral_to_start;

 Timer t;

float maximum_current = MAXIMUM_CURRENT_SINGLE;

//Create the PID class

int reverse_flag=0;
float motor_current = 0;
bool wasStopped = true;
bool startUpEnd = false;
unsigned long startUpEndTime = 0;
unsigned long lastIncrementTime=0;
float sweepCurrent = 40;

void init_CAN_m();
void send_CAN_m(void* context); 
void neutral_to_start_f();

void setup ()
{
	//status = 1;
	neutral_to_start = false;
	Serial.begin (115200) ;   // NOTE BAUD RATE

	init_CAN_m();

	if( Can0.begin(CAN_BPS_500K)){
		Serial.println("CAN Init OK!");
	}
	else{
		Serial.println("CAN Init Failed!");
	}
	Can0.attachObj(&Can_tsb);
	
	Can0.setNumTXBoxes(1);    // Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.
	
	Can0.watchFor();
	// Can0.setRXFilter(0x1b01,0x1b01,true);
	Can_tsb.attachGeneralHandler();

	

	pinMode(13, OUTPUT);
	if (!enc.begin ()){
		Serial.println ("Error setting up AS5045") ;
	}
	
	/* led HIGH until start sequence is done */
	neutral_to_start_f(); 
	Serial.print("Exit start");

	/* Let LED_BUILTIN toggle every 1000th millisecond with start
	condition HIGH */
	t.oscillate(LED_BUILTIN, 1000, HIGH);

	t.every(100,send_CAN_m,(void*)1);

}

void loop ()
{
	angle = (int) round(enc.read() * 0.08789);
	//Serial.println(angle);
	if( angle < 285)
		angle += 360;
	// /*reverse*/// 50    -> 352 - 410
	if ( angle >= 356){
	if (wasStopped)
			throttle = map( angle ,415 ,356,-100,-20);  // maped 
		else
			throttle = map( angle ,415,356,-100,-5); 
	}
	// /*neutral*/ 350 351 
	if (angle==355 ){				
		throttle = 0;
	wasStopped = true;
	}
	/*forward*/ // 289  -> 289 - 349
	if (angle<355){
		if (wasStopped)
			throttle = map( angle ,354,285,20,100);								
		else
			throttle = map( angle ,354,285,5,100);
	}

	 if (wasStopped == true && throttle != 0 && (millis() - lastIncrementTime) > 100 ){
	// 	// Serial.println(sweepCurrent);
	 	lastIncrementTime = millis();
		motor1.setMotorCurrent(sweepCurrent);
	 	sweepCurrent += 1;
	 	if (sweepCurrent > 50 || Can_tsb.motor.rpm > 1000 ){
	 		startUpEnd = true;
	 		wasStopped = false;
	 		startUpEndTime = millis();
	 		sweepCurrent = 40;
	 	}
		
	}
	 else{
	motor_current = throttle * maximum_current/100;
		if(motor_current < 0) reverse_flag = 1; else reverse_flag = 0;
	//	
		switch(reverse_flag){
			case 0: // If going forward - here with PID
	//motor1.setMotorCurrent(motor_current);
	motor1.setMotorCurrent(motor_current);
			break;
			case 1: // If going in reverse divide current by 2
				motor1.setMotorCurrent(motor_current/2);
			break;
	}
	}      
	
	if(Can_tsb.motor.voltage < 30)
		neutral_to_start_f();

	// Serial.println((String)"kp: " + speedPID.GetKp() + " ki: " + speedPID.GetKi() + " kd: " + speedPID.GetKd());
	/* Updates the timer*/
	//Serial.println(Can_tsb.motor.voltage);
	t.update(); 
}

void neutral_to_start_f( ){

	status = 0;
	while(neutral_to_start != true || Can_tsb.motor.voltage<30){
		
		
		motor1.setMotorCurrent(0);

		start_angle = (int) round(enc.read()*0.08789);
		send_CAN_m((void*)1);

		delay(250);
		
		start_angle2 = (int) round(enc.read()*0.08789);
		send_CAN_m((void*)1);

		if ((start_angle == 355 || start_angle == 356) && (start_angle2 == 355 || start_angle2 == 356)){ 
			neutral_to_start = true;
		}

		if ((start_angle < 355 || start_angle > 356) && (start_angle2 < 355 || start_angle2 > 356)){ 
			neutral_to_start = false;
		}
		delay(250);
		//Serial.println((String) " Here: " + Can_tsb.motor.voltage + " " + neutral_to_start + " " + start_angle + " " +  start_angle2);
 
	}

	/* initial sequence ends*/
	status = 1; 
	
}

void init_CAN_m(){

	can_message.id=0x10;
	can_message.len = 2;
}

void send_CAN_m(void* context){
	/* Serial.print ("\t");
	Serial.print("Throttle: ");
	Serial.print ("\t");
	Serial.print(angle);
	Serial.print ("\t");
	Serial.println (throttle,DEC); */
	int32_t ind = 0;
	
	buffer_append_int8(can_message.buf,throttle,&ind);
	buffer_append_uint8(can_message.buf,status,&ind);
	Can0.write(can_message);
	// Serial.println(throttle);
}


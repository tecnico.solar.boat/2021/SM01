var mode = getVariableValue("mode"); //mode ==1: vorgabe, mode ==2: player, mode == 0: init
var pattern = getVariableValue("pattern");
var level = getVariableValue ("level");
var play_level = getVariableValue ("play_level");
var blink = getVariableValue ("blink");
var blinklevel = getVariableValue ("blinklevel");
var repeatrate = getVariableValue ("repeatrate");
var waittimer = getVariableValue ("waittimer");

var buttonbase = 720;
var show_move = getVariableValue("show_move");
switch (mode)
{
    case 0:
        setVariableValue("buttons_off",1);
        setVariableValue("@Visibility00",0);
        var color_number = Math.floor(Math.random()*(4)) + 1;
        switch (color_number)
        {
            case 1: pattern = pattern + "a"; break;//rot
            case 2: pattern = pattern + "b"; break;//blau
            case 3: pattern = pattern + "c"; break;//gelb
            case 4: pattern = pattern + "d"; break;//gr�n
        }
        setVariableValue ("mode",1);
        setVariableValue ("pattern",pattern);
        setVariableValue ("blinklevel",0);
        setVariableValue ("blink",0);
        break;
    
    case 1:
        if (waittimer == repeatrate)
        {
            setVariableValue("buttons_off",1);
            var current_color = pattern[blinklevel];
            switch (blink)
            {
            case 0: 
                switch (current_color)
                {
                case "a": 
                    current_color_number = 1; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 255;
                    break;
                case "b": 
                    current_color_number = 2; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [2] = 255;
                    break;
                case "c": 
                    current_color_number = 3; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 255;
                    color [1] = 255;
                    break;
                case "d": 
                    current_color_number = 4; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [1] = 255;
                    break;    
                }
                setProperty(buttonbase+current_color_number,"Background Color",color);
                blink = 1;
                setVariableValue("show_move",blinklevel+1);
                setVariableValue ("waittimer",1);
                break;
                
            case 1:
                switch (current_color)
                {
                case "a": 
                    current_color_number = 1; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 100;
                    break;
                case "b": 
                    current_color_number = 2; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [2] = 100;
                    break;
                case "c": 
                    current_color_number = 3; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 100;
                    color [1] = 100;
                    break;
                case "d": 
                    current_color_number = 4; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [1] = 100;
                    break;    
                }
                setVariableValue ("waittimer",1);
                setProperty(buttonbase+current_color_number,"Background Color",color);
                blinklevel++;
                if (blinklevel == level)//fertig mit zeigen, input mode
                {
                    setVariableValue("mode",2);
                    setVariableValue("@Visibility00",4);
                    setVariableValue("show_move",0);
                    setVariableValue("buttons_off",0);
                    blinklevel = 0;
                }
                else//es kommt noch eine Zahl
                {
                    setVariableValue("buttons_off",1);
                }
                blink = 0;
                break;
            }
        }
        else
        {
            setVariableValue ("waittimer",waittimer+1);
        }
        setVariableValue ("blink",blink);
        setVariableValue ("blinklevel",blinklevel);
        setVariableValue ("play_level",1);
        setVariableValue ("button_pressed","f");
        break;
    
    case 2:
        var button = getVariableValue ("button_pressed");
        var buttons_off = getVariableValue ("buttons_off");
        var right_button = pattern [play_level-1];
        switch (button)
        {
            case "f"://warten
            break;
            
            case right_button://richtig
                setVariableValue("@Visibility00",0);
                setVariableValue("show_move",show_move+1);
                setVariableValue ("play_level",play_level +1);
                setVariableValue ("button_pressed","f");
                if (play_level == level)//alles richtig, neues level
                {
                    setVariableValue("show_move",show_move+1);
                    setVariableValue ("level",level +1);
                    setVariableValue ("mode",0);
                    setVariableValue ("blinklevel",0);
                    setVariableValue ("button_pressed","f");
                    setVariableValue ("play_level",1);
                }
                break;
                
            default://falsch
                var number_games = getVariableValue ("number_games");
                setVariableValue("show_move",0);
                setVariableValue ("number_games",number_games+1);
                setVariableValue("@Visibility00",1);
                setVariableValue("buttons_off",1);
                var record = getVariableValue(("record" + repeatrate));
                if (record < level-1)
                {
                    setVariableValue(("record" + repeatrate),level-1);
                }
                setVariableValue ("mode",3);
		setProperty(719, "Flashing", true);
                setVariableValue("stage_vis", 0);
        }
        break;  
    case 4:
        break;
}
    





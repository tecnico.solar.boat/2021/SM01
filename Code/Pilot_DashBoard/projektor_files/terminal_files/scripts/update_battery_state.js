// Update battery_state_string based on battery_state
var status = getVariableValue("Battery_state_status");
var temp = getVariableValue("Battery_state_temp");
var volt = getVariableValue("Battery_state_volt");

var charger_status;

switch (status) {
    case 0:
        setVariableValue("Battery_state_status_string", "Battery");
        charger_status = 0;
        break;
    case 1:
        setVariableValue("Battery_state_status_string", "FC + Charge");
        charger_status = 1;
        break;
    case 2:
        setVariableValue("Battery_state_status_string", "FC");
        charger_status = 0;
        break;
    default:
        setVariableValue("Battery_state_status_string", "[N/A]");
        charger_status = 0;
        break;
}

setVariableValue("Charger_Status", charger_status);


switch (temp) {
    case 0:
        setVariableValue("Battery_state_temp_string", "Normal");
        break;
    case 1:
        setVariableValue("Battery_state_temp_string", "Over Temp");
        break;
    case 2:
        setVariableValue("Battery_state_temp_string", "Under Temp");
        break;
    default:
        setVariableValue("Battery_state_temp_string", "[N/A]");
        break;
}

switch (volt) {
    case 0:
        setVariableValue("Battery_state_volt_string", "Normal");
        break;
    case 1:
        setVariableValue("Battery_state_volt_string", "Over Volt");
        break;
    case 2:
        setVariableValue("Battery_state_volt_string", "Under Volt");
        break;
    default:
        setVariableValue("Battery_state_volt_string", "[N/A]");
        break;
}
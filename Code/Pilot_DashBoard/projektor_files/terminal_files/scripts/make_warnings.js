// Make warnings
var warn_overvolt = 0;
var warn_undervolt = 0;
var warn_overtemp70 = 0;
var warn_overtemp80 = 0;

var High_cell_volt = getVariableValue("High_cell_volt");
var Low_cell_volt = getVariableValue("Low_cell_volt");
var Temp_Cooling_Out = getVariableValue("Temp_Cooling_Out");
var Temp_Cooling_In = getVariableValue("Temp_Cooling_In");

if (High_cell_volt > 0.8){
    warn_overvolt = 1;
}

if (Low_cell_volt < 0.3){
    warn_undervolt = 1;
}

if (Temp_Cooling_Out > 70 || Temp_Cooling_In > 70){
    warn_overtemp70 = 1;
}

if (Temp_Cooling_Out > 80 || Temp_Cooling_In > 80){
    warn_overtemp80 = 1;
}

setVariableValue("warn_overvolt", warn_overvolt);
setVariableValue("warn_undervolt", warn_undervolt);
setVariableValue("warn_overtemp70", warn_overtemp70);
setVariableValue("warn_overtemp80", warn_overtemp80);
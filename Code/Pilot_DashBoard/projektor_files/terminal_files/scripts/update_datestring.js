// Create date string based at RTC

// RTC Clock
year = getVariableValue("@RTC_Year");
month = getVariableValue("@RTC_Month");
day = getVariableValue("@RTC_Day");
hour = getVariableValue("@RTC_Hours");
min = getVariableValue("@RTC_Minutes");
sec = getVariableValue("@RTC_Seconds");


date = new Date(year, month, day, hour, min, sec);
time_string = date.toLocaleTimeString(); //hour+':'+min+':'+sec;
date_string = date.toLocaleDateString();
setVariableValue("Date_string", date_string);
setVariableValue("Time_string", time_string);

/*
print("Date1: " + date.toLocaleString());
print("Date1: " + date.toString());
print("Date1: " + date.toLocaleTimeString());
print("Date1: " + date.toLocaleDateString());
*/



setVariableValue("level",1);
setVariableValue("mode",0);
setVariableValue("pattern","");
setVariableValue("blink",0);
setVariableValue("blinklevel",0);
setVariableValue("button_pressed","f");
setVariableValue("buttons_off",1);
setVariableValue("@Visibility00",2);
setVariableValue("show_move",0);
setVariableValue("play_level",1);
setVariableValue("Help_Vis", 0);
setVariableValue("stage_vis", 1);

var color = getProperty(721,"Background Color");
color [0] = 100;
color [1] = 0;
color [2] = 0;
setProperty(721,"Background Color",color);

var color = getProperty(722,"Background Color");
color [0] = 0;
color [1] = 0;
color [2] = 100;
setProperty(722,"Background Color",color);

var color = getProperty(723,"Background Color");
color [0] = 100;
color [1] = 100;
color [2] = 0;
setProperty(723,"Background Color",color);

var color = getProperty(724,"Background Color");
color [0] = 0;
color [1] = 100;
color [2] = 0;
setProperty(724,"Background Color",color);
setProperty(719, "Flashing", false);




#! /bin/sh

UNZIP_FILE="$APP_DATA_PARTITION/pclient/projekte/default_prj/terminal_files.zip"
UNZIP_PATH="$APP_DATA_PARTITION/pclient/projekte/default_prj/"

UNZIP_FONT_FILE="$APP_DATA_PARTITION/pclient/projekte/default_prj/imported_fonts.zip"
UNZIP_FONT_PATH="$APP_DATA_PARTITION/pclient/projekte/default_prj/imported_fonts/"
ALL_FONTS="$APP_DATA_PARTITION/pclient/projekte/default_prj/imported_fonts/*"
TARGET_FONT_PATH="$APP_DATA_PARTITION""/share/fonts/"
#ALL_FONTS="$APP_DATA_PARTITION/pclient/projekte/default_prj/imported_fonts/*"

BOOT_LOGO_SRC="$APP_DATA_PARTITION/pclient/projekte/default_prj/terminal_files/additional_files/logo.bmp"
BOOT_LOGO_DEST="/boot/logo.bmp"

SERVICE_LOGO_SRC="$APP_DATA_PARTITION/pclient/projekte/default_prj/terminal_files/additional_files/recovery.bmp"
SERVICE_LOGO_DEST="/boot/recovery.bmp"

SYSTEM_LOGO_SRC="$APP_DATA_PARTITION/pclient/projekte/default_prj/terminal_files/additional_files/background.png"
SYSTEM_LOGO_DEST="$APP_DATA_PARTITION/share/background.png"
SHARE_PATH="$APP_DATA_PARTITION/share/"

UNZIP_CMD="unzip"

if [ -z "$APP_DATA_PARTITION/pclient/projekte/default_prj/terminal_files.zip" ];then
	UNZIP_FILE="$APP_DATA_PARTITION/sample.zip"
	echo "default unzip file"	
else
	echo "supplied argument"
fi

if [ -z "$APP_DATA_PARTITION/pclient/projekte/default_prj/" ];then
	UNZIP_PATH="$APP_DATA_PARTITION/sample"
	echo "default unzip path"	
else
	echo "supplied argument"
fi

echo "Unzipping.."
export PATH="$APP_DATA_PARTITION/ud4/bin:$PATH"
mkdir -p "$UNZIP_PATH"
"$UNZIP_CMD" "$UNZIP_FILE" -d "$UNZIP_PATH"

# if archiv exist
if [ -f $UNZIP_FONT_FILE ];then

	echo "Unzipping Fonts.."
	"$UNZIP_CMD" "$UNZIP_FONT_FILE" -d "$UNZIP_PATH"

	# create a font directory if not exist
	mkdir -p $TARGET_FONT_PATH

	# copy the new fonts into the directory
	cp $ALL_FONTS $TARGET_FONT_PATH
	
	# remove the transfered temp directories for fonts
	rm -rf $UNZIP_FONT_PATH
fi


sync
# set new boot logo if file logo.bmp is available in folder 'terminal_files/additional_files/'
if [ -f $BOOT_LOGO_SRC ];then
	echo "setting new boot logo..."
	if [ -f "/usr/sbin/setbootlogo" ];then
		# set boot logo os 2.x.x
		setbootlogo $BOOT_LOGO_SRC
	else
		# set boot logo os 1.x.x (egspro)
		cp $BOOT_LOGO_SRC $BOOT_LOGO_DEST
	fi
fi

# set new service logo if file recovery.bmp is available in folder 'terminal_files/additional_files/'
if [ -f $SERVICE_LOGO_SRC ];then
	echo "setting new service logo..."
        cp $SERVICE_LOGO_SRC $SERVICE_LOGO_DEST
fi

# set new system background logo if file background.png is available in folder 'terminal_files/additional_files/'
if [ -f $SYSTEM_LOGO_SRC ];then

        if [ -d $SHARE_PATH ];then
                echo "Folder exists: $SHARE_PATH"
        else
                # create a share directory
                mkdir -p $SHARE_PATH
        fi

	echo "setting new system backround logo..."
        cp $SYSTEM_LOGO_SRC $SYSTEM_LOGO_DEST
fi

sync

exit 0

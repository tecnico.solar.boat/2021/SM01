/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "TMB.h"

/**
 * Initializes all pins
*/
TMB::TMB(double *temp_micronel, double *airflow)
{
    /* Start subClasses */
    hts = new Hts();
    relays = new Relays(TMB::Pin::CTRL_SOL1, TMB::Pin::CTRL_SOL2, TMB::Pin::CTRL_WATERPUMP, TMB::Pin::CTRL_SPARERELAY);
    knf = new Knf();
    micronel = new Micronel(temp_micronel, airflow);
}

/**
 * Concatenates all status variables in Teensy main board into a uint8_t
 * @return {int}  : [X X X X X Sol2 Sol1 WaterPump]
 */
int TMB::readStates()
{
    int states = 0;

    states |= relays->getState_sol2();
    states = states << 1;
    states |= relays->getState_sol1();
    states = states << 1;
    states |= relays->getState_waterPump();

    return states;
}

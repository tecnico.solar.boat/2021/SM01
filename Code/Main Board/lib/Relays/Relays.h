/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef RELAYS_h
#define RELAYS_h

#include <Arduino.h>

/**
 * Struct that holds all the information about the relays
*/
struct Relays
{
private:
    int _pin_sol1;
    bool _state_sol1 = LOW;
    int _pin_sol2;
    bool _state_sol2 = LOW;
    int _pin_waterPump;
    bool _state_waterPump = LOW;
    int _pin_spare;
    bool _state_spare = LOW;

public:
    Relays(int, int, int, int);
    void setState_sol1(bool);
    void setState_sol2(bool);
    void setState_waterPump(bool);
    void setState_spare(bool);

    /**
     * Get current state of solenoid 1
    * @return {bool}  : current state of solenoid
    */
    bool getState_sol1() { return _state_sol1; }

    /**
 * Get current state of solenoid 2
     * @return {bool}  : current state of solenoid
     */
    bool getState_sol2() { return _state_sol2; }

    /**
     * Get current state of water pump
     * @return {bool}  : current state of water pump  
        */
    bool getState_waterPump() { return _state_waterPump; }

    /**
         * Get current state of spare relay 
         * @return {bool}  : current state of spare relay
         */
    bool getState_spare() { return _state_spare; }
};
#endif
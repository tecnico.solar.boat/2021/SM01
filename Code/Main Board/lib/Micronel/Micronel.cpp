/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Wire.h>
#include "Micronel.h"
#include "../PID_v1/src/PID_v1.h"

/**
 * Class constructer
 * 
 * @param  {double*} temperatureInside : pointer to current blower temperature 
 * @param  {double*} currentAirflow    : pointer to current air flow in system
 */
Micronel::Micronel(double *temperatureInside, double *currentAirflow)
{
    /* MCP4725 start */
    dac.begin();
    dac.setVoltage(0, false);

    // Save variables
    _currentAirFlow = currentAirflow;
    _currentTemp = temperatureInside;
}

/**
 * Micronel start up rotine.
 */
void Micronel::start()
{
    // Only start the blower if the temperature is under 50.0C
    if (*_currentTemp < _MAXSTARTTEMP)
    {
        Serial.println("Stating pump");
        // Send 2 volts to start the pump
        dac.setVoltage(1638, false);
        delay(500);
        // Start at minimum speed
        dac.setVoltage(1235, false);
        _state = 1;
    }
}

/**
 * Micronel stop rotine 
 */
void Micronel::stop()
{
    Serial.println("Stoping pump...");
    dac.setVoltage(0, false);
    _state = 0;
}

/**
 * Return voltage in in uint16_t inside micronel limits 
 * @return {uint16_t}  : normalized voltage
 */
uint16_t Micronel::getVoltage16(double voltageDouble)
{
    uint16_t voltage = 0;
    double voltaged;
    /**
     * input minimum = 1.5
     * input maximum = 4
     * 
     * output minimum = 1229
     * output maximum = 3276
    */
    voltaged = map(voltageDouble, 1.5, 4.0, 1229.0, 3276.0);
    voltage = (int)voltaged;
    return voltage;
}

void Micronel::setVoltage(uint16_t voltage)
{
    dac.setVoltage(voltage, false);
}
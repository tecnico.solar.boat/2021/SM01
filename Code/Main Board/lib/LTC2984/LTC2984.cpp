#include "LTC2984.h"

void configure_channels()
{
    uint8_t channel_number;
    uint32_t channel_assignment_data;

    // ----- Channel 2: Assign Sense Resistor -----
    channel_assignment_data =
        SENSOR_TYPE__SENSE_RESISTOR |
        (uint32_t)0x61A8000 << SENSE_RESISTOR_VALUE_LSB; // sense resistor - value: 100000.
    assign_channel(CHIP_SELECT, 2, channel_assignment_data);
    // ----- Channel 4: Assign Thermistor Custom Table -----
    channel_assignment_data =
        SENSOR_TYPE__THERMISTOR_CUSTOM_TABLE |
        THERMISTOR_RSENSE_CHANNEL__2 |
        THERMISTOR_DIFFERENTIAL |
        THERMISTOR_EXCITATION_MODE__SHARING_ROTATION |
        THERMISTOR_EXCITATION_CURRENT__1UA |
        (uint32_t)0x0 << THERMISTOR_CUSTOM_ADDRESS_LSB | // thermistor - custom address: 0.
        (uint32_t)0xF << THERMISTOR_CUSTOM_LENGTH_1_LSB; // thermistor - custom length-1: 15.
    assign_channel(CHIP_SELECT, 4, channel_assignment_data);
    // ----- Channel 6: Assign Thermistor Custom Table -----
    channel_assignment_data =
        SENSOR_TYPE__THERMISTOR_CUSTOM_TABLE |
        THERMISTOR_RSENSE_CHANNEL__2 |
        THERMISTOR_DIFFERENTIAL |
        THERMISTOR_EXCITATION_MODE__SHARING_ROTATION |
        THERMISTOR_EXCITATION_CURRENT__1UA |
        (uint32_t)0x10 << THERMISTOR_CUSTOM_ADDRESS_LSB | // thermistor - custom address: 16.
        (uint32_t)0xF << THERMISTOR_CUSTOM_LENGTH_1_LSB;  // thermistor - custom length-1: 15.
    assign_channel(CHIP_SELECT, 6, channel_assignment_data);
    // ----- Channel 8: Assign Sense Resistor -----
    channel_assignment_data =
        SENSOR_TYPE__SENSE_RESISTOR |
        (uint32_t)0x9C4000 << SENSE_RESISTOR_VALUE_LSB; // sense resistor - value: 10000.
    assign_channel(CHIP_SELECT, 8, channel_assignment_data);
    // ----- Channel 10: Assign Thermistor Custom Table -----
    channel_assignment_data =
        SENSOR_TYPE__THERMISTOR_CUSTOM_TABLE |
        THERMISTOR_RSENSE_CHANNEL__8 |
        THERMISTOR_DIFFERENTIAL |
        THERMISTOR_EXCITATION_MODE__SHARING_ROTATION |
        THERMISTOR_EXCITATION_CURRENT__1UA |
        (uint32_t)0x20 << THERMISTOR_CUSTOM_ADDRESS_LSB | // thermistor - custom address: 32.
        (uint32_t)0x7 << THERMISTOR_CUSTOM_LENGTH_1_LSB;  // thermistor - custom length-1: 7.
    assign_channel(CHIP_SELECT, 10, channel_assignment_data);
    // ----- Channel 12: Assign Thermistor Custom Table -----
    channel_assignment_data =
        SENSOR_TYPE__THERMISTOR_CUSTOM_TABLE |
        THERMISTOR_RSENSE_CHANNEL__8 |
        THERMISTOR_DIFFERENTIAL |
        THERMISTOR_EXCITATION_MODE__SHARING_ROTATION |
        THERMISTOR_EXCITATION_CURRENT__1UA |
        (uint32_t)0x28 << THERMISTOR_CUSTOM_ADDRESS_LSB | // thermistor - custom address: 40.
        (uint32_t)0x7 << THERMISTOR_CUSTOM_LENGTH_1_LSB;  // thermistor - custom length-1: 7.
    assign_channel(CHIP_SELECT, 12, channel_assignment_data);
    // ----- Channel 14: Assign Thermistor Custom Table -----
    channel_assignment_data =
        SENSOR_TYPE__THERMISTOR_CUSTOM_TABLE |
        THERMISTOR_RSENSE_CHANNEL__8 |
        THERMISTOR_DIFFERENTIAL |
        THERMISTOR_EXCITATION_MODE__SHARING_ROTATION |
        THERMISTOR_EXCITATION_CURRENT__1UA |
        (uint32_t)0x30 << THERMISTOR_CUSTOM_ADDRESS_LSB | // thermistor - custom address: 48.
        (uint32_t)0x7 << THERMISTOR_CUSTOM_LENGTH_1_LSB;  // thermistor - custom length-1: 7.
    assign_channel(CHIP_SELECT, 14, channel_assignment_data);
    // ----- Channel 15: Assign Direct ADC -----
    channel_assignment_data =
        SENSOR_TYPE__DIRECT_ADC |
        DIRECT_ADC_SINGLE_ENDED;
    assign_channel(CHIP_SELECT, 15, channel_assignment_data);
    // ----- Channel 16: Assign Direct ADC -----
    channel_assignment_data =
        SENSOR_TYPE__DIRECT_ADC |
        DIRECT_ADC_SINGLE_ENDED;
    assign_channel(CHIP_SELECT, 16, channel_assignment_data);
    // ----- Channel 17: Assign Direct ADC -----
    channel_assignment_data =
        SENSOR_TYPE__DIRECT_ADC |
        DIRECT_ADC_SINGLE_ENDED;
    assign_channel(CHIP_SELECT, 17, channel_assignment_data);
    // ----- Channel 18: Assign Direct ADC -----
    channel_assignment_data =
        SENSOR_TYPE__DIRECT_ADC |
        DIRECT_ADC_SINGLE_ENDED;
    assign_channel(CHIP_SELECT, 18, channel_assignment_data);
    // ----- Channel 19: Assign Direct ADC -----
    channel_assignment_data =
        SENSOR_TYPE__DIRECT_ADC |
        DIRECT_ADC_SINGLE_ENDED;
    assign_channel(CHIP_SELECT, 19, channel_assignment_data);
    // ----- Channel 20: Assign Direct ADC -----
    channel_assignment_data =
        SENSOR_TYPE__DIRECT_ADC |
        DIRECT_ADC_SINGLE_ENDED;
    assign_channel(CHIP_SELECT, 20, channel_assignment_data);
}

void configure_memory_table()
{
    uint16_t start_address;
    uint16_t table_length;
    // int i;

    // -- Channel 4 custom table --
    table_coeffs ch_4_coefficients[] =
        {
            {128736, 366746},  // -- 8046.0, 358.15
            {154384, 361626},  // -- 9649.0, 353.15
            {185920, 356506},  // -- 11620.0, 348.15
            {225120, 351386},  // -- 14070.0, 343.15
            {273920, 346266},  // -- 17120.0, 338.15
            {334720, 341146},  // -- 20920.0, 333.15
            {411360, 336026},  // -- 25710.0, 328.15
            {508320, 330906},  // -- 31770.0, 323.15
            {631680, 325786},  // -- 39480.0, 318.15
            {789440, 320666},  // -- 49340.0, 313.15
            {992800, 315546},  // -- 62050.0, 308.15
            {1256160, 310426}, // -- 78510.0, 303.15
            {1600000, 305306}, // -- 100000.0, 298.15
            {2051200, 300186}, // -- 128200.0, 293.15
            {2649600, 295066}, // -- 165600.0, 288.15
            {3448000, 289946}  // -- 215500.0, 283.15
        };
    start_address = (uint16_t)592; // Real address = 6*0 + 0x250 = 592
    table_length = (uint8_t)16;    // Real table length = 15 + 1 = 16
    write_custom_table(CHIP_SELECT, ch_4_coefficients, start_address, table_length);
    // -- Channel 6 custom table --
    table_coeffs ch_6_coefficients[] =
        {
            {128736, 366746},  // -- 8046.0, 358.15
            {154384, 361626},  // -- 9649.0, 353.15
            {185920, 356506},  // -- 11620.0, 348.15
            {225120, 351386},  // -- 14070.0, 343.15
            {273920, 346266},  // -- 17120.0, 338.15
            {334720, 341146},  // -- 20920.0, 333.15
            {411360, 336026},  // -- 25710.0, 328.15
            {508320, 330906},  // -- 31770.0, 323.15
            {631680, 325786},  // -- 39480.0, 318.15
            {789440, 320666},  // -- 49340.0, 313.15
            {992800, 315546},  // -- 62050.0, 308.15
            {1256160, 310426}, // -- 78510.0, 303.15
            {1600000, 305306}, // -- 100000.0, 298.15
            {2051200, 300186}, // -- 128200.0, 293.15
            {2649600, 295066}, // -- 165600.0, 288.15
            {3448000, 289946}  // -- 215500.0, 283.15
        };
    start_address = (uint16_t)688; // Real address = 6*16 + 0x250 = 688
    table_length = (uint8_t)16;    // Real table length = 15 + 1 = 16
    write_custom_table(CHIP_SELECT, ch_6_coefficients, start_address, table_length);
    // -- Channel 10 custom table --
    table_coeffs ch_10_coefficients[] =
        {
            {5411, 402586},   // -- 338.2, 393.15
            {10850, 382106},  // -- 678.1, 373.15
            {17136, 366746},  // -- 1071.0, 358.15
            {39840, 341146},  // -- 2490.0, 333.15
            {85296, 320666},  // -- 5331.0, 313.15
            {160000, 305306}, // -- 10000.0, 298.15
            {199776, 300186}, // -- 12486.0, 293.15
            {521056, 279706}  // -- 32566.0, 273.15
        };
    start_address = (uint16_t)784; // Real address = 6*32 + 0x250 = 784
    table_length = (uint8_t)8;     // Real table length = 7 + 1 = 8
    write_custom_table(CHIP_SELECT, ch_10_coefficients, start_address, table_length);
    // -- Channel 12 custom table --
    table_coeffs ch_12_coefficients[] =
        {
            {5411, 402586},   // -- 338.2, 393.15
            {10850, 382106},  // -- 678.1, 373.15
            {17136, 366746},  // -- 1071.0, 358.15
            {39840, 341146},  // -- 2490.0, 333.15
            {85296, 320666},  // -- 5331.0, 313.15
            {160000, 305306}, // -- 10000.0, 298.15
            {199776, 300186}, // -- 12486.0, 293.15
            {521056, 279706}  // -- 32566.0, 273.15
        };
    start_address = (uint16_t)832; // Real address = 6*40 + 0x250 = 832
    table_length = (uint8_t)8;     // Real table length = 7 + 1 = 8
    write_custom_table(CHIP_SELECT, ch_12_coefficients, start_address, table_length);
    // -- Channel 14 custom table --
    table_coeffs ch_14_coefficients[] =
        {
            {5411, 402586},   // -- 338.2, 393.15
            {10850, 382106},  // -- 678.1, 373.15
            {17136, 366746},  // -- 1071.0, 358.15
            {39840, 341146},  // -- 2490.0, 333.15
            {85296, 320666},  // -- 5331.0, 313.15
            {160000, 305306}, // -- 10000.0, 298.15
            {199776, 300186}, // -- 12486.0, 293.15
            {521056, 279706}  // -- 32566.0, 273.15
        };
    start_address = (uint16_t)880; // Real address = 6*48 + 0x250 = 880
    table_length = (uint8_t)8;     // Real table length = 7 + 1 = 8
    write_custom_table(CHIP_SELECT, ch_14_coefficients, start_address, table_length);
}

void configure_global_parameters()
{
    // -- Set global parameters
    transfer_byte(CHIP_SELECT, WRITE_TO_RAM, 0xF0, TEMP_UNIT__C | REJECTION__50_60_HZ);
    // -- Set any extra delay between conversions (in this case, 0*100us)
    transfer_byte(CHIP_SELECT, WRITE_TO_RAM, 0xFF, 0);
}

//----------------------LTC2984_support_functions.cpp--------------------------------------

// ***********************
// Program the part
// ***********************
void assign_channel(uint8_t chip_select, uint8_t channel_number, uint32_t channel_assignment_data)
{
    uint16_t start_address = get_start_address(CH_ADDRESS_BASE, channel_number);
    transfer_four_bytes(chip_select, WRITE_TO_RAM, start_address, channel_assignment_data);
}

void write_custom_table(uint8_t chip_select, struct table_coeffs coefficients[64], uint16_t start_address, uint8_t table_length)
{
    int8_t i;
    uint32_t coeff;

    output_low(chip_select);

    SPI.transfer(WRITE_TO_RAM);
    SPI.transfer(highByte(start_address));
    SPI.transfer(lowByte(start_address));

    for (i = 0; i < table_length; i++)
    {
        coeff = coefficients[i].measurement;
        SPI.transfer((uint8_t)(coeff >> 16));
        SPI.transfer((uint8_t)(coeff >> 8));
        SPI.transfer((uint8_t)coeff);

        coeff = coefficients[i].temperature;
        SPI.transfer((uint8_t)(coeff >> 16));
        SPI.transfer((uint8_t)(coeff >> 8));
        SPI.transfer((uint8_t)coeff);
    }
    output_high(chip_select);
}

void write_custom_steinhart_hart(uint8_t chip_select, uint32_t steinhart_hart_coeffs[6], uint16_t start_address)
{
    int8_t i;
    uint32_t coeff;

    output_low(chip_select);

    SPI.transfer(WRITE_TO_RAM);
    SPI.transfer(highByte(start_address));
    SPI.transfer(lowByte(start_address));

    for (i = 0; i < 6; i++)
    {
        coeff = steinhart_hart_coeffs[i];
        SPI.transfer((uint8_t)(coeff >> 24));
        SPI.transfer((uint8_t)(coeff >> 16));
        SPI.transfer((uint8_t)(coeff >> 8));
        SPI.transfer((uint8_t)coeff);
    }
    output_high(chip_select);
}

// ******************************
// EEPROM transfer
// ******************************
void eeprom_transfer(uint8_t chip_select, uint8_t eeprom_read_or_write)
// Read from or write to the EEPROM.
// To read from the EEPROM, pass READ_FROM_EEPROM into eeprom_read_or_write.
// To write to the EEPROM, pass WRITE_TO_EEPROM into eeprom_read_or_write.
{
    uint8_t eeprom_status;
    Serial.println("** EEPROM transfer started ** ");

    // Set EEPROM key
    transfer_four_bytes(chip_select, WRITE_TO_RAM, EEPROM_START_ADDRESS, EEPROM_KEY);

    // Set EEPROM read/write
    transfer_byte(chip_select, WRITE_TO_RAM, COMMAND_STATUS_REGISTER, eeprom_read_or_write);

    // Wait for read/write to finish
    wait_for_process_to_finish(chip_select);

    // Check for success
    eeprom_status = transfer_byte(chip_select, READ_FROM_RAM, EEPROM_STATUS_REGISTER, 0);
    if (eeprom_status == 0)
    {
        Serial.println("** EEPROM transfer succeeded ** ");
    }
    else
    {
        Serial.print(F("** EEPROM transfer had a problem. Status byte ="));
        Serial.println(eeprom_status);
    }
}

// *****************
// Measure channel
// *****************
void measure_channel(uint8_t chip_select, uint8_t channel_number, uint8_t channel_output)
{
    convert_channel(chip_select, channel_number);
    get_result(chip_select, channel_number, channel_output);
}

void convert_channel(uint8_t chip_select, uint8_t channel_number)
{
    // Start conversion
    transfer_byte(chip_select, WRITE_TO_RAM, COMMAND_STATUS_REGISTER, CONVERSION_CONTROL_BYTE | channel_number);

    wait_for_process_to_finish(chip_select);
}

void wait_for_process_to_finish(uint8_t chip_select)
{
    uint8_t process_finished = 0;
    uint8_t data;
    while (process_finished == 0)
    {
        data = transfer_byte(chip_select, READ_FROM_RAM, COMMAND_STATUS_REGISTER, 0);

        process_finished = data & 0x40;
    }
}

// *********************************
// Get results
// *********************************
void get_result(uint8_t chip_select, uint8_t channel_number, uint8_t channel_output)
{
    uint32_t raw_data;
    uint8_t fault_data;
    uint16_t start_address = get_start_address(CONVERSION_RESULT_MEMORY_BASE, channel_number);
    uint32_t raw_conversion_result;

    raw_data = transfer_four_bytes(chip_select, READ_FROM_RAM, start_address, 0);

    //Serial.print(F("\nChannel "));
    //Serial.println(channel_number);

    // 24 LSB's are conversion result
    raw_conversion_result = raw_data & 0xFFFFFF;
    if (channel_number == 4)
    {
        // round to int
        TEMP_MICRONEL = round(double(raw_conversion_result) / 1024);
    }
    else if (channel_number == 6)
    {
        TEMP_100K_SPARE = float(raw_conversion_result) / 1024;
    }
    else if (channel_number == 10)
    {
        // Round to int
        TEMP_WC_IN = round(float(raw_conversion_result) / 1024);
    }
    else if (channel_number == 12)
    {
        // Round to int
        TEMP_WC_OUT = round(float(raw_conversion_result) / 1024);
    }
    else if (channel_number == 14)
    {
        TEMP_10K_SPARE = (float(raw_conversion_result) / 1024);
    }
    else if (channel_number == 15)
    {
        SPARE_ADC_4 = (float(raw_conversion_result) / 2097152);
    }
    else if (channel_number == 16)
    {
        SPARE_ADC_2 = (float(raw_conversion_result) / 2097152);
    }
    else if (channel_number == 17) // AIRFLOW
    {
        SPARE_ADC_1 = (double(raw_conversion_result) / 2097152) * 2;
        
        // Convert from V to L/min and rounds double with one digit precision
        SPARE_ADC_1 = round(((67.7436*SPARE_ADC_1*SPARE_ADC_1) - (325.9748*SPARE_ADC_1) + 392.0629)*10)/10; 
    }
    else if (channel_number == 18)
    {
        SPEED_AR = (float(raw_conversion_result) / 2097152);
    }
    else if (channel_number == 19)
    {
        SPARE_ADC_3 = (float(raw_conversion_result) / 2097152);
    }
    else if (channel_number == 20)
    {
        AIRFLOW = (double(raw_conversion_result) / 2097152);
    }

    //print_conversion_result(raw_conversion_result, channel_output);

    // If you're interested in the raw voltage or resistance, use the following
    if (channel_output != VOLTAGE)
    {
        read_voltage_or_resistance_results(chip_select, channel_number);
    }

    // 8 MSB's show the fault data
    fault_data = raw_data >> 24;
    //print_fault_data(fault_data);
}

void print_conversion_result(uint32_t raw_conversion_result, uint8_t channel_output)
{
    int32_t signed_data = raw_conversion_result;
    float scaled_result;

    // Convert the 24 LSB's into a signed 32-bit integer
    if (signed_data & 0x800000)
        signed_data = signed_data | 0xFF000000;

    // Translate and print result
    if (channel_output == TEMPERATURE)
    {
        scaled_result = float(signed_data) / 1024;
        Serial.print(F("  Temperature = "));
        Serial.println(scaled_result);
    }
    else if (channel_output == VOLTAGE)
    {
        scaled_result = float(signed_data) / 2097152;
        Serial.print(F("  Direct ADC reading in V = "));
        Serial.println(scaled_result);
    }
}

void read_voltage_or_resistance_results(uint8_t chip_select, uint8_t channel_number)
{
    int32_t raw_data;
    float voltage_or_resistance_result;
    uint16_t start_address = get_start_address(VOUT_CH_BASE, channel_number);

    raw_data = transfer_four_bytes(chip_select, READ_FROM_RAM, start_address, 0);
    voltage_or_resistance_result = (float)raw_data / 1024;
    //Serial.print(F("  Voltage or resistance = "));
    //Serial.println(voltage_or_resistance_result);
}

// Translate the fault byte into usable fault data and print it out
void print_fault_data(uint8_t fault_byte)
{
    //
    // Serial.print(F("  FAULT DATA = "));
    // Serial.println(fault_byte, BIN);

    if (fault_byte & SENSOR_HARD_FAILURE)
        Serial.println(F("  - SENSOR HARD FALURE"));
    if (fault_byte & ADC_HARD_FAILURE)
        Serial.println(F("  - ADC_HARD_FAILURE"));
    if (fault_byte & CJ_HARD_FAILURE)
        Serial.println(F("  - CJ_HARD_FAILURE"));
    if (fault_byte & CJ_SOFT_FAILURE)
        Serial.println(F("  - CJ_SOFT_FAILURE"));
    if (fault_byte & SENSOR_ABOVE)
        Serial.println(F("  - SENSOR_ABOVE"));
    if (fault_byte & SENSOR_BELOW)
        Serial.println(F("  - SENSOR_BELOW"));
    if (fault_byte & ADC_RANGE_ERROR)
        Serial.println(F("  - ADC_RANGE_ERROR"));
    if (!(fault_byte & VALID))
        Serial.println(F("INVALID READING !!!!!!"));
    if (fault_byte == 0b11111111)
        Serial.println(F("CONFIGURATION ERROR !!!!!!"));
}

// *********************
// SPI RAM data transfer
// *********************
// To write to the RAM, set ram_read_or_write = WRITE_TO_RAM.
// To read from the RAM, set ram_read_or_write = READ_FROM_RAM.
// input_data is the data to send into the RAM. If you are reading from the part, set input_data = 0.

uint32_t transfer_four_bytes(uint8_t chip_select, uint8_t ram_read_or_write, uint16_t start_address, uint32_t input_data)
{
    uint32_t output_data;
    uint8_t tx[7], rx[7];

    tx[6] = ram_read_or_write;
    tx[5] = highByte(start_address);
    tx[4] = lowByte(start_address);
    tx[3] = (uint8_t)(input_data >> 24);
    tx[2] = (uint8_t)(input_data >> 16);
    tx[1] = (uint8_t)(input_data >> 8);
    tx[0] = (uint8_t)input_data;

    spi_transfer_block(chip_select, tx, rx, 7);

    output_data = (uint32_t)rx[3] << 24 |
                  (uint32_t)rx[2] << 16 |
                  (uint32_t)rx[1] << 8 |
                  (uint32_t)rx[0];

    return output_data;
}

uint8_t transfer_byte(uint8_t chip_select, uint8_t ram_read_or_write, uint16_t start_address, uint8_t input_data)
{
    uint8_t tx[4], rx[4];

    tx[3] = ram_read_or_write;
    tx[2] = (uint8_t)(start_address >> 8);
    tx[1] = (uint8_t)start_address;
    tx[0] = input_data;
    spi_transfer_block(chip_select, tx, rx, 4);
    return rx[0];
}

// ******************************
// Misc support functions
// ******************************
uint16_t get_start_address(uint16_t base_address, uint8_t channel_number)
{
    return base_address + 4 * (channel_number - 1);
}

bool is_number_in_array(uint8_t number, uint8_t *array, uint8_t array_length)
// Find out if a number is an element in an array
{
    bool found = false;
    for (uint8_t i = 0; i < array_length; i++)
    {
        if (number == array[i])
        {
            found = true;
        }
    }
    return found;
}

//----------------------LTC2984_Setup--------------------------------------

void LTC2894_Setup()
{
    quikeval_SPI_init();          // Configure the spi port for 4MHz SCK
    quikeval_SPI_connect();       // Connect SPI to main data port
    pinMode(CHIP_SELECT, OUTPUT); // Configure chip select pin on Linduino

    configure_channels();
    configure_memory_table();
    configure_global_parameters();

    Serial.println("LTC2984 Initialized");
}
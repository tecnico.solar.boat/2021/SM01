/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Wire.h>
#include "Hts.h"

/**
 * Class construct 
*/
Hts::Hts()
{
    // Start Sensors
    OutH2.begin();
    //InAir.begin();
}

/** 
 * Read data from all sensors and store them in variables
 */
void Hts::getData()
{
    /* Reads data from sensor at hidrogen outlet */
    OutH2.readSensor();
    _hOutH2 = OutH2.humidity;
    _tOutH2 = OutH2.temperatureC;

    /* Reads data from sensor at air inlet */
    /* InAir.readSensor();
    _hInAir = InAir.humidity;
    _tInAir = InAir.temperatureC; */
}

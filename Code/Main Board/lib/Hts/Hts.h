/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef HTS_h
#define HTS_h

#include "CFF_ChipCap2.h"

/**
 * Struct that holds all the information about humidity and temperature sensor
*/
struct Hts
{
private:
    //CFF_ChipCap2 InAir = CFF_ChipCap2();
    float _hInAir = 0;
    float _tInAir = 0;

    //CFF_ChipCap2 InH2 = CFF_ChipCap2();
    float _hInH2 = 0;
    float _tInH2;

    CFF_ChipCap2 OutH2 = CFF_ChipCap2();
    float _hOutH2 = 0;
    float _tOutH2 = 0;

public:
    Hts();
    void getData();

    /**
         * Get the humidity at air inlet
         * @return {int}  : current humidity at air inlet
         */
    int getHInAir() { return round(_hInAir); }
    /**
         * Get the temperature at air inlet
         * @return {int}  : temperature at air inlet
         */
    int getTInAir() { return round(_tInAir); }

    /**
         * Get the humidity at hidrogen inlet
         * @return {int}  : current humidity at hidrogen inlet
         */
    int getHInH2() { return round(_hInH2); }
    /**
         * Get the temperature at hidrogen inlet
         * @return {int}  : current temperature at hidrogen inlet
         */
    int getTInH2() { return round(_tInH2); }

    /**
         * Get the humidity at hidrogen outlet
         * @return {int}  : current humidity at hidrogen outlet
         */
    int getHOutH2() { return round(_hOutH2); }
    /**
         * Get the temperature at hidrogen outlet
         * @return {int}  : current temperature at hidrogen outlet
         */
    int getTOutH2() { return round(_tOutH2); }
};

#endif
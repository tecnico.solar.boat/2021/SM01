/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include <IntervalTimer.h>

#include "CAN_TSB_H2.h"

#include "LTC2984.h"
#include "../../include/Linduino.h"

#include "TMB.h"

// Constants to test
#define MIN_AIRFLOW 135
#define MAX_AIRFLOW 300
#define MAX_FC_TEMP 75
#define MAX_FC_START_TEMP 70
#define SAMPLE_AIRFLOW 5
#define TIME_BETWEEN_SOL_BLOWER 4000000
#define TIME_BETWEEN_CAN_MESSAGES 1000000
#define KNF_SPEED 2000 // 500 3000 -> 2000

// LTC2894 variables
float TEMP_100K_SPARE, TEMP_WC_IN, TEMP_WC_OUT, TEMP_10K_SPARE, SPARE_ADC_4, SPARE_ADC_3, SPARE_ADC_2, SPEED_AR;
double TEMP_MICRONEL, AIRFLOW, SPARE_ADC_1;

//Temp variables
byte byteR;

// Teensy main board object
TMB *tmb = new TMB(&TEMP_MICRONEL, &SPARE_ADC_1);
bool powerMode = 0;
bool preCharge = 0;
bool motor = 0;

// Can Object
TSB_CAN_H2 _canMH;
FlexCAN_T4<CAN0, RX_SIZE_256, TX_SIZE_16> Can0;

// Messages to send
CAN_message_t _HB_Temp_Humid, _HB_Pressure_Status, _Talk_To_Bat;

// Bat command's id
uint8_t command [3] = {0x14, 0x15, 0x1A}; 
int currentCommand = 0;

// Timer for sending can messages
IntervalTimer sendCanMessagesTimer;

// Timer for changing the current state of the blower
IntervalTimer blowerTimer, relayTimer;

// Target airflow PID variables
double target2 = 0.65;
double targetAirflow = MIN_AIRFLOW;
double avgCellVolt;
double kp2 = 15;
double ki2 = 0;
double kd2 = 0;
PID targetAirflowPID(&avgCellVolt, &targetAirflow, &target2, kp2, ki2, kd2, DIRECT);

// Airflow PID variables
double target = 155;
double voltage;
double kp = 0.00412055013206722;
double ki = 0.0230573278926859;
double kd = 0;
PID micronelPID(&SPARE_ADC_1, &voltage, &targetAirflow, kp, ki, kd, DIRECT);

// Functions
void readChannels();

void startUp();
void shutDown();

void startBlower();
void stopBlower();
void openRelay();
void blowerLogic();

void setupCan();
void setupCanMessages();
void sendCanMessages();

bool isKillswitchOn();

void setup()
{
    // Start serial
    Serial.begin(9600);

    delay(5000);
    Serial.println("Started");

    pinMode(QUIKEVAL_CS, OUTPUT);
    LTC2894_Setup();

    // Setup Can
    setupCan();
    setupCanMessages();

    // Start Timer
    sendCanMessagesTimer.begin(sendCanMessages, TIME_BETWEEN_CAN_MESSAGES);

    // Set PID limits and turn PID off
    micronelPID.SetOutputLimits(1.5, 4.0);
    micronelPID.SetMode(MANUAL);
    targetAirflowPID.SetOutputLimits(MIN_AIRFLOW, MAX_AIRFLOW);
    targetAirflowPID.SetMode(MANUAL);
}

void loop()
{
    //-----------------------//
    // Controls Blower state //
    //-----------------------//
    if (_canMH.power_box.Killswitch == 1 && powerMode == 0)
    {
        Serial.println("Starting FC if temp is under MAX_FC_START_TEMP");
        if (TEMP_WC_IN <= MAX_FC_START_TEMP && TEMP_WC_OUT <= MAX_FC_START_TEMP)
        {
            startUp();
        }
    }
    else if (_canMH.power_box.Killswitch == 0 && powerMode == 1)
    {
        shutDown();
    }

    //--------------//
    // Reading data //
    //--------------//
    readChannels();
    tmb->hts->getData();
    // Update avg cell voltage
    avgCellVolt = _canMH.fc.average_volt;

    //----------------//
    // Control blower //
    //----------------//
    blowerLogic();

    //--------------------------//
    // check if fc temp is good
    //--------------------------//
    if ((TEMP_WC_OUT > MAX_FC_TEMP || TEMP_WC_IN > MAX_FC_TEMP) && powerMode == 1)
    {
        shutDown();
    }

    // TEMPORARY CONTROL OF TARGET AIRFLOW
    if (Serial.available())
    {
        byteR = Serial.read();
        if (byteR == 'w')
        {
            if (target < 300)
            {
                target += 5;
                Serial.print("Target = ");
                Serial.println(target);
            }
        }
        else if (byteR == 's')
        {
            if (target > 135)
            {
                target -= 5;
                Serial.print("Target = ");
                Serial.println(target);
            }
        }
        else if (byteR == 'o')
        {
            preCharge = 1;
            motor = 1;
        }
        else if (byteR == 'c')
        {
            preCharge = 0;
            motor = 0;
        }
        else if (byteR == 'h')
        {
            // Start and stop water Pump
            tmb->relays->setState_waterPump(!tmb->relays->getState_waterPump());
        }
        else if (byteR == 'm')
        {
            // Start and stop H2 solonoid
            tmb->relays->setState_sol1(!tmb->relays->getState_sol1());
        }
        else if (byteR == 'j')
        {
            // Start and stop H20 solonoid
            tmb->relays->setState_sol2(!tmb->relays->getState_sol2());
        }
        else if (byteR == 'r')
        {
            tmb->knf->start();
            tmb->knf->setSpeed(KNF_SPEED);
        }
        else if (byteR == 'u')
        {
            tmb->knf->stop();
        }
        else if (byteR == 'a')
        {
            // Start blower at lowest voltage
            tmb->micronel->start();

            // Start PID at lowest airflow and lowest voltage
            target = MIN_AIRFLOW; // Remove this line of code when we know what to do with the target airflow
            voltage = 1.5;
            micronelPID.SetMode(AUTOMATIC);
        }
        else if (byteR == 'z')
        {
            // Stop blower
            tmb->micronel->stop();

            // Stop PID
            micronelPID.SetMode(MANUAL);
        }
    }
}

/*
 * Read all LTC2894 channels and store them in external variables
 */
void readChannels()
{
    measure_channel(CHIP_SELECT, 4, TEMPERATURE); // Ch 4: Blower Temp
    measure_channel(CHIP_SELECT, 17, VOLTAGE);

    // read airflow sensor Sample_airflow times and make a mean
    /* float total = 0;
    for (int i = 0; i < SAMPLE_AIRFLOW; i++)
    {
        measure_channel(CHIP_SELECT, 17, VOLTAGE);
        total += SPARE_ADC_1;
    }
    SPARE_ADC_1 = total / SAMPLE_AIRFLOW;
 */
    measure_channel(CHIP_SELECT, 10, TEMPERATURE); // Ch 10: Cooling temp
    measure_channel(CHIP_SELECT, 12, TEMPERATURE); // Ch 12: Cooling temp

    //Print sensor data here
}

// START FC
void startUp()
{
    // Close relay and open H2 solonoid and H20 solonoid
    tmb->relays->setState_sol1(HIGH);
    //tmb->relays->setState_sol2(HIGH);
    tmb->relays->setState_waterPump(HIGH);

    // Start Knf pump
    tmb->knf->start();
    delay(50);
    tmb->knf->setSpeed(KNF_SPEED);

    // IF ManualBlower is on start blower after TIME_BETWEEN_SOL_BLOWER
    // Start blower after TIME_BETWEEN_SOL_BLOWER
    blowerTimer.begin(startBlower, TIME_BETWEEN_SOL_BLOWER);

    powerMode = 1;
}

// STOP FC
void shutDown()
{
    // Stop blower
    stopBlower();

    // Open relay and close H2 solonoid after TIME_BETWEEN_SOL_BLOWER
    relayTimer.begin(openRelay, TIME_BETWEEN_SOL_BLOWER);

    powerMode = 0;
}

void startBlower()
{
    // Start blower at lowest voltage
    tmb->micronel->start();

    // Start PID at lowest airflow and lowest voltage
    voltage = 1.5;
    micronelPID.SetMode(AUTOMATIC);

    // Start PID that controls target airflow
    targetAirflow = MIN_AIRFLOW;
    targetAirflowPID.SetMode(AUTOMATIC);

    // Only exec once this function
    blowerTimer.end();
}

void stopBlower()
{
    // Stop blower
    tmb->micronel->stop();

    // Stop PID
    micronelPID.SetMode(MANUAL);
    targetAirflowPID.SetMode(MANUAL);
}

void openRelay()
{
    // Open relay
    tmb->relays->setState_sol1(LOW);
    //tmb->relays->setState_sol2(LOW);
    tmb->relays->setState_waterPump(LOW);
    tmb->knf->stop();

    // Only exec once this function
    relayTimer.end();
}

void blowerLogic()
{
    // Check if blower is on
    if (tmb->micronel->getState() == 1)
    {
        // IF MANUALBLOWER == 0: StopBlower
        // Check if temperature inside blower is under the threshold
        if (tmb->micronel->isTempUnderThreshold())
        {   
            // Calculate new target airflow
            targetAirflowPID.Compute();

            if (micronelPID.Compute())
            {
                // If a new output was computed change blower ouput
                tmb->micronel->setVoltage(tmb->micronel->getVoltage16(voltage));
            }
        }
        else
        {
            // If temperature is over the threshold stop FC
            shutDown();
        }
    }
    // ELSE IF POWERMODE == 1 && MANUALBLOWER IS ON 
    // Start
}

/**
 * Initializes CAN objects and configures them 
 */
void setupCan()
{
    Can0.begin();
    Can0.setBaudRate(500000);

    Can0.setMaxMB(16);
    Can0.enableMBInterrupts();

    Can0.attachObj(&_canMH);

    _canMH.attachGeneralHandler();
}

/**
 * Initializes CAN message objects and configures them 
 */
void setupCanMessages()
{
    // Blowwer + status of solenoids and Pump
    _HB_Pressure_Status.id = 0x629;
    _HB_Pressure_Status.len = 8;

    // Temperatures of water cooling and temperatures&Humiditys of air+h2
    _HB_Temp_Humid.id = 0x619;
    _HB_Temp_Humid.len = 8;
    
    // Message used to talk to bat
    _Talk_To_Bat.id = 0x201;
    _Talk_To_Bat.len = 8;
}

void sendCanMessages()
{
    int32_t ind = 0;

    // HB_Pressure_Status (CAN ID: 0x629)
    buffer_append_int8(_HB_Pressure_Status.buf, 0, &ind);                 // Pressure H2 Inlet
    buffer_append_int8(_HB_Pressure_Status.buf, 0, &ind);                 // Pressure H2O Outlet
    buffer_append_uint16(_HB_Pressure_Status.buf, 0, &ind);               // RPMs of Air blower
    buffer_append_int16(_HB_Pressure_Status.buf, SPARE_ADC_1 * 10, &ind); // Air flow Inlet
    buffer_append_uint8(_HB_Pressure_Status.buf, TEMP_MICRONEL, &ind);    // Blower temp
    buffer_append_int8(_HB_Pressure_Status.buf, tmb->readStates(), &ind); // Status bits

    Can0.write(_HB_Pressure_Status);

    ind = 0;

    // HB_Temp_Humid (CAN ID: 0x619)
    buffer_append_uint8(_HB_Temp_Humid.buf, tmb->hts->getTInH2(), &ind);  // Temperature H2 Inlet
    buffer_append_uint8(_HB_Temp_Humid.buf, tmb->hts->getTOutH2(), &ind); // Temperature H2 Outlet
    buffer_append_uint8(_HB_Temp_Humid.buf, tmb->hts->getHInH2(), &ind);  // Humidity H2 Inlet
    buffer_append_uint8(_HB_Temp_Humid.buf, tmb->hts->getHOutH2(), &ind); // Humidity H2 Outlet
    buffer_append_uint8(_HB_Temp_Humid.buf, tmb->hts->getTInAir(), &ind); // Temperature Air Inlet
    buffer_append_uint8(_HB_Temp_Humid.buf, tmb->hts->getHInAir(), &ind); // Humidity Air Inlet
    buffer_append_uint8(_HB_Temp_Humid.buf, TEMP_WC_IN, &ind);            // Temperature Cooling Inlet
    buffer_append_uint8(_HB_Temp_Humid.buf, TEMP_WC_OUT, &ind);           // Temperature Cooling Outlet

    Can0.write(_HB_Temp_Humid);

    ind = 0;

    //Request bat voltage
    buffer_append_uint8(_Talk_To_Bat.buf, command[currentCommand], &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);
    buffer_append_uint8(_Talk_To_Bat.buf, 0, &ind);

    Can0.write(_Talk_To_Bat);

    currentCommand++;
    if (currentCommand == 3)
    {
        currentCommand=0;
    }
}

/**
 * NOT USED
 * 
 * Returns if blower needs to turn on or off based on the killswitch
 * @return {bool}  : 1 if blower needs to be ON, 0 if blower needs to be OFF
 */
bool isKillswitchOn()
{
    // To start Blower pre_charge relay needs to be open
    if (preCharge == 1)
    {
        return 1;
    }
    // If pre_charge is OFF but Motor is ON
    else if (motor == 1 && preCharge == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}